
isPalindrome(process.argv.slice(2, process.argv.length));

function isPalindrome(input) {
  if (!input) {
    console.log('Usage: node is-palindrome.js <your-word-or-phrase>');
    return;
  }
  
  if(input.join(' ') === input.join(' ').split('').reverse().join('')) {
    console.log(true);
  }
  else {
    console.log(false);
  }

}


/*
// official implementation

function isPalindrome(string) {
  string = string.toLowerCase();
  var charactersArr = string.split('');
  var validCharacters = 'abcdefghijklmnopqrstuvwxyz'.split('');
  
  var lettersArr = [];
  charactersArr.forEach(char => {
    if (validCharacters.indexOf(char) > -1) lettersArr.push(char);
  });
  
  return lettersArr.join('') === lettersArr.reverse().join('');
}
 
isPalindrome("Madam, I'm Adam");
*/