# Is Palindrome

It takes a string as an argument and returns:

* **true** if the string is a palindrome;

* **false** otherwise.

A **palindrome** is any word or phrase that is spelled the same way both backward and forward.