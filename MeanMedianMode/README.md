# Mean Median Mode

* It takes in a **number array** as a parameter;

* It returns an **object** that has 3 properties:

  * **mean** of all the numbers in the array;

  * **median** which is the median of all the numbers in;

  * **mode** of all the numbers in the array.

```javascript
{
  mean: 'the_mean_val',
  median: 'the_median_val',
  mode: 'the_mode_val(s)'
}
```

## Functional Programming

This exercises allows to practice **functional programming** that is the practice of breaking down your algorithms and your code into separate functions, so that these separate functions can be re-used in multiple different places so that you don+t have to rewriting the same code.