
meanMedianMode(process.argv.slice(2));

function getMean(array) {
  var reducer = (accumulator, currentValue) => accumulator + currentValue;
  
  mean = array.reduce(reducer) / array.length;
  return mean;
}

function getMedian(array) {
  array.sort();

  var median;
  if (array.length % 2 !== 0) {
    median = array[Math.floor(array.length / 2)];
  }
  else {
    var mid1 = array[(array.length /2) -1];
    var mid2 = array[array.length /2];
    median = (mid1 + mid2) / 2;
  }
  return median;
}

function getMode(array) {
  // return what number or numbers appear most in the array
  var modeObject = {};

  array.forEach( num => {
    if(!modeObject[num]) {
      modeObject[num] = 0;
    }
    modeObject[num]++;
  });

  // loop through every property or number on object and
  // find out which number or number appear most frequenttly
  var maxFrequency = 0; // how often the current mode shows up
  var modes = []; // to put all of mods in

  for (var num in modeObject) {
    // check if the frequency of that number is greater than the max frequency
    if (modeObject[num] > maxFrequency) {
      modes = [num];
      maxFrequency = modeObject[num];
    }
    // if the current number has the same frequency as the max frequency
    else if(modeObject[num] === maxFrequency) {
      // if yes, push the current number into the modes array
      modes.push(num);
    }
  }

  // if every number appears at the same frequency or the same number of times
  // in that case there would be no mode
  
  // if the number of modes in our mode array equals the number of keys and our mode object
  // then we know that all of the numbers apper to the same frequency, so there is no mode
  if (modes.length === Object.keys(modeObject).length) {
    modes = [];
  }

  return modes;

}

function meanMedianMode(array) {
  if (!array) {
    console.log('Usage: node mean-median-mode.js \'<your-number-array>\'');
    return;
  }

  array = array[0].split(' ');

  var resultObject = {
    mean: getMean(array),
    median: getMedian(array),
    mode: getMode(array)
  }
  console.log(resultObject);

}
