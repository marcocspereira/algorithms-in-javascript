# Merge Sort

```bash
# original array
[11 7 4 1 15 12 3]
         / \
[11 7 4 1] [15 12 3]
    / \         /   \
[11 7][4 1] [15 12]  [3]
   / \   /\     / \   \
[11][7][4][1] [15][12][3]
  \ |   | /    \  /   /
[7 11] [1 4] [12 15] [3]
   |    /      |     / 
[1 4 7 11]   [3 12 15]
    \           /
 [ 1 3 4 7 11 12 15]
```

```javascript
function mergeSort(array) {
  // take in a single, unsorted array as a parameter
  // split the array into two halves
}
```

```javascript
function merge (array1, array2) {
  // takes in two sorted arrays as parameters
  // merges those sorted arrays into one sorted array
  // returns one sorted array
}
```