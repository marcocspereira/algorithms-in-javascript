
# Fibonacci

It is simply a special sequence of numbers called the **Fibonacci Sequence**:

```
1, 1, 2, 3, 5, ,8, 13, 21, 34 ...
```

The first two numbers in the sequence are 1 and 1 and every number after that is the sum of the previous two numbers.

We will be building an algorithm called Fibonacci and it will take in a *number* called **position** as a parameter. This position will indicate which number from the Fibonacci sequence we want returned to us.

```javascript
fibonacci(4); // returns 3
fibonacci(9); // returns 34
```

## Hints

* Does not require a lot of code;

* Base case deals with the fact that the first two numbers are always 1 and 1.

## Solution

```javascript
function fibonacci(position) {
  // base case
  if(position < 3) {
    return 1;
  }

  // recursive case
  return fibonacci(position-2) + fibonacci(position-1);

}
```

However, this implementation has a very bad time complexity because every time we call Fibonacci function, we call it twice more inside the function.

This results in a time complexity of **O (2^n)**, which is an **exponential time complexity**.

It is not a good algorithm to implement in practice but it is a very good algorithm to know for technical job interviews.

---

# Memoized Fibonnaci, the fix

This algorithm will take in two parameters:

* **index** of number in fibonacci sequence:

* **cache**, an array used as memory.

The original implementation recalculate the same numbers over and over again and this was a complete waste of time and very inefficient.

But when we use the memoization method to produce a Fibonacci sequence, we don't have to waste time doing all of these calculations over and over again.

## Memoization:

* Check to see if number already exists in cache:

  * If number **is** in cache, use that number;
  
  * If number **is not** in cache, calculate it and put in cache so it can be used multiple times in future.

This results in a time complexity of **O (n)**, which is an **linear time complexity**, since every number will only be calculated once.