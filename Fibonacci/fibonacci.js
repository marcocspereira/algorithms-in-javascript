
main(process.argv[2]);

function main(position) {

  if (!parseInt(position)) {
    console.log('Usage: node fibonacci.js <your-num-position>');
    return;
  }

  console.log(fibonacci(position));
  
}

function fibonacci(position) {
  // base case
  if(position < 3) {
    return 1;
  }

  // recursive case
  return fibonacci(position-2) + fibonacci(position-1);

}

