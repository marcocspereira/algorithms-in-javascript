
main(process.argv[2]);

function main(position) {

  if (!parseInt(position)) {
    console.log('Usage: node memoized-fibonacci.js <your-num-position>');
    return;
  }

  console.log(fibonacciMemo(position, []));
  
}

function fibonacciMemo(index, cache) {

  cache = cache || [];

  // base case
  if (cache[index]) {
    return cache[index];
  }
  else {
    // base case
    if(index < 3) {
      return 1;
    }
    cache[index] = fibonacciMemo(index - 1, cache) + fibonacciMemo(index - 2, cache);
  }

  return cache[index];
}

