
main(process.argv[2], process.argv[3]);

function main(numArray, key) {
  
  if (!numArray || !parseInt(key)) {
    console.log('Usage: node binary-search.js \'<your-num-array-separated-with-spaces>\' <your-sum-value>');
    console.log('Example: node binary-search.js \'5 7 12 16 36 39 42 56 71\' 56');
    return;
  }

  numArray = numArray.split(' ');

  console.log(binarySearch(numArray, key));
}

function binarySearch(numArray, key) {
  key = parseInt(key);
  
  // define the middle index of the array because we know that
  // for binary search, we split the given array at the middle
  var middleIndex = Math.floor(numArray.length / 2);
  var middleElement = parseInt(numArray[middleIndex]);

  // base case
  if (middleElement === key) {
    return true;
  }
  // 1st recursive case
  else if (middleElement < key && numArray.length > 1) {
    return binarySearch(numArray.splice(middleIndex, numArray.length), key);
  }
  // 2nd recursive case
  else if (middleElement > key && numArray.length > 1) {
    return binarySearch(numArray.splice(0, middleIndex), key);
  }
  // only when our middle element is not the key we are looking for
  else {
    return false;
  }
}


// '5 7 12 16 36 39 42 56 71' 56