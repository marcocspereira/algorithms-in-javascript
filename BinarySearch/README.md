
# Binary Search

It takes two arguments:

* numArray;

* key

It returns:

* **true** if the key is present in the numArray;

* **false**, otherwise.

An important fact to remember about binary search: **the given array must be sorted**. So, for this exercise, we will assume that any number passed into the algorithm is sorted by value from least to greatest.

A binary search algorithm allows a user to search for a given value inside of a list.

- It searches for a given value (key) inside of a list (numArray);

- Runs in **O (log n)** run time, which is very performant.

4000 elements -> 12 operations

There are different ways to code out binary search algorithms, but this exercise it implements a **recursive way**. Check **HarmlessRansomNote** *README.md* to check another implementation.

* It takes two parameters:

  * a *numbers* **array** which we will call **numArray**;

  * **number** which we will call **sum**.

This algorithm should return an **array filled with every pair of numbers**.

## Recursion and the Call Stack

Recursion is when a function calls itself.

```javascript
function func() {
  it (/* base case */) {
    return something;
  }
  else {  // recursive case
    func();
  }
}

// factorial (!)
// 4! = 4 * 3 * 2 * 1 = 24
function factorial(n) {
  if (n === 1) {
    return n;
  }
  else{
    return n * factorial(n-1)
  }
}
```

### Call Stack

For the given example:

```bash
$ factorial(4)
24
```

factorial(4)

```
num = 4
return 4 * factorial(3)
```

factorial(3)

```
num = 3
return 3 * factorial(2)
```

factorial(2)

```
num = 2
return 2 * factorial(1)
```

factorial(1)

```
num = 1
return 1
```

From now here, the call stack starts to unwind and everything will start to make sense.

factorial(2)

```
num = 2
return 2 * 1
```

factorial(3)

```
num = 3
return 3 * 2
```

factorial(4)

```
num = 4
return 4 * 6
```

**Result:** 24