
# Sieve of Eratosthenes

The purpose of this algorithm will be to find all the prime numbers up to a given number.

```javascript
sieveOfEratosthenes(20);
// should return [2, 3, 5, 7, 11, 13, 17, 19]
```

The way this algorithm works starts with a **list** of numbers from **0** to **n**;

```javascript
sieveOfEratosthenes(20);
```

In this case we would start with an array of numbers from 0 to 20:

```javascript
[0 , 1 ,2 ,3 ,4 ,5 ,6 ,7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
```

And initially, we are going to mark them all as prime to begin with. So we use the indexes of the array as the given number and we are going to make every element of the array the value **true**. Like this:

```javascript
      [true, true, true, true, true, true, true, true, true, true,
Index: 0   , 1   , 2   , 3   , 4   , 5   , 6   , 7   , 8   , 9   
       true, true, true, true, true, true, true, true, true, true, true ]
Index: 10  , 11  , 12  , 13  , 14  , 15  , 16  , 17  , 18  , 19  , 20
```

Then we will go through our array and mark every number that is not actually Prime as false.

* First we mark 0 and 1 as false, because we know that they are both not prime numbers.

```javascript
      [false, false, true, true, true, true, true, true, true, true,
Index: 0    , 1    , 2   , 3   , 4   , 5   , 6   , 7   , 8   , 9   
       true, true, true, true, true, true, true, true, true, true, true ]
Index: 10  , 11  , 12  , 13  , 14  , 15  , 16  , 17  , 18  , 19  , 20
```

* Then, we are going to loop through the rest of the list. On every number we hit, we are going to mark all of its mulitples as false because we know that any multiple of a number cannot be prime.

Let's star with **2**.

```javascript
      [false, false, true, true, false, true, false, true, false, true,
Index: 0    , 1    , 2   , 3   , 4    , 5   , 6    , 7   , 8    , 9   
       false, true, false, true, false, true, false, true, false, true, false ]
Index: 10   , 11  , 12   , 13  , 14   , 15  , 16   , 17  , 18   , 19  , 20
```

* Then we will move on **3** and we will mark all of its multiples as false.

```javascript
      [false, false, true, true, false, true, false, true, false, false,
Index: 0    , 1    , 2   , 3   , 4    , 5   , 6    , 7   , 8    , 9   
       false, true, false, true, false, false, false, true, false, true, false ]
Index: 10   , 11  , 12   , 13  , 14   , 15   , 16   , 17  , 18   , 19  , 20
```

And so on.

## Optimization

Stop looping through at the squer root of **num**.

We can stop our loop at the squer root of num because all non-prime numbers after the square root of num will be marked as false by the time we get to the square root of num. So there is no point in continuing our loop.