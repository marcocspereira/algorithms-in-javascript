
sieveOfEratosthenes(process.argv[2]);


function sieveOfEratosthenes(num) {

  if (!parseInt(num)) {
    console.log('Usage: node sieve-of-eratosthenes.js <your-num-position>');
    return;
  }

  var primes = [];
  for(var i=0; i<=num; i++){
    primes[i] = true;
  }
  
  primes[0] = false;
  primes[1] = false;

  for(var i=2; i<=Math.sqrt(num); i++) {
    // inner loop is a way we handle marking the multiples of each number we passas false
    // j*i because is each a multiple of the current index or I
    for(var j=2; j*i<=num; j++) {
      primes[i*j] = false;
    }
  }

  var result = [];

  for (var i=0; i<primes.length; i++) {    
    if(primes[i]) {
      result.push(i);
    }
  }

  console.log(result);

}

