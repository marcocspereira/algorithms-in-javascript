fizzBuzz(process.argv.slice(2));

justFizzBuzz(process.argv.slice(2));

function fizzBuzz(num) {
  if (!parseInt(num)) {
    console.log('Usage: node fizz-buzz.js <your_int_number>');
    return;
  }

  var fizz = 3;
  var buzz = 5;
  var fizzbuzz = fizz * buzz;

  for (var i = 1; i <= num; i++) {
    result = '';
    if ( i % fizzbuzz === 0 ) {
      console.log('FizzBuzz');
    }
    else if ( i % fizz === 0 ) {
      console.log(result = 'Fizz');
    }
    else if ( i % buzz === 0 ) {
      console.log(result + 'Buzz');
    }
    else {
      console.log(i);
    }
  }
}

// just print if the given num is fizz, buzz, fizzbuzz or nothing
function justFizzBuzz(num) {
  if (!parseInt(num)) {
    console.log('Usage: node fizz-buzz.js <your_int_number>');
    return;
  }
  
  var fizz = 3;
  var buzz = 5;
  result = '';

  if ( num % fizz === 0 ) {
    result = 'Fizz';
  }
  if ( num % buzz === 0 ) {
    result += 'Buzz';
  }

  console.log (result || parseInt(num));

}