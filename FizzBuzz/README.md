# Fizz Buzz

* A function that takes in a number as a parameter which we call **num**.

* The functions logs out to the console:

  * every number from **1** to **num**;

  * for each number:

    * if is **divisible by 3** we want to log out the word **Fizz**;
    * if is **divisible by 5** we want to log out the word **Buzz**;
    * if is **divisible by 3** and **divisible by 5** we want to log out the word **FizzBuzz**;

## Example

```bash
# usage
$ node fizz-buzz.js 20
# result
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
17
Fizz
19
Buzz
```

## Modulus Operator

**Description:** It gives the remainder of a number after that number has been divided by another number.

**Operator:** %

**Usage:** a % b

Do *a* divided by *b* then return to us the *remainder* from that.

### Example

```shell
$ console.log(7 % 3);
1
$ console.log(100 % 30);
10
```