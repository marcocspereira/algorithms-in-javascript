# Algorithms in JavaScript

* Based on [this Udemy course](https://www.udemy.com/learning-algorithms-in-javascript-from-scratch/);
* Each algorithm has a description and the source code.

## List

* FizzBuzz

* Harmless Ransom Note

* Is Palindrome

* Caesar Cipher

* Reverse Words

* Reverse Array in Place

* Mean Median Mode

* Two Sum

* Binary Search

* Fibonacci

* Memoized Fibonacci

* Sieve of Eratosthenes

* Bubble Sort

* Merge Sort

* Max Stock Profit
