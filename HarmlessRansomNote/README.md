# Harmless Ransom Note

* By building this algorithm, we can learn some techniques that are critical to writing fast in performant programs such as how to use an object as a hash table;

* The Harmless Ransom Note it is simply a note that is made from words cut out of a magazine.

* The function receives two arguments: **noteText** and **magazineText**.

* The purpose of this algorithm is to tell us if we have enough words in our magazine to create our note. If we do, the algorithm will return **true**. If not it will return **False**.

## Example

**Note:**

'*this is a secret note for you from a secret admirer*'

**Magazine:**

'*puerto rico is a great place you must hike far from town to find a secret waterfall that i am an admirer of but note that is not as hard as it seems this is my advice for you*'

**Output:**

**False**, because the word '*secret*' is only found once in the magazine but we use it twice in the note.

**Note:**

'*this is a note for you from a secret admirer*'

**Magazine:**

'*puerto rico is a great place you must hike far from town to find a secret waterfall that i am an admirer of but note that is not as hard as it seems this is my advice for you*'

**Output:**

**True**, because all of the words that we need can be found in the magazine.

---

## Time Complexity and Big O Notation

It describes how fast an algorithm runs.

### Big O Notation

It is used to classify how **scalable** an algorithm or function is. It also allows us to estimate the worst case a runtime of an algorithm or how long it takes the algorithm to complete based on the input size. This means that Big O informs us of how much slower an algorithm will run if it's input size grows.

#### O(1) -> Constant runtime

```javascript
function log(array) {
  console.log(array[0]);
  console.log(array[1]);
}
```

The reuntime of this functions is going to be **constant**, because as the input size increases (or in this case as we increase the size of the array), the **number of operations** that we perform **never changes**.

#### O(n) -> Linear runtime

```javascript
function logAll(array) {
  for(var i=0; i<array.length; i++) {
    console.log(array[i]);
  }
}
```

We have to do an operation on every single element in the array. So, as the input size increases, our runtime will also increase. For this function, our **runtime** will **increase proportionally** to how much our input increases.

This is called **linear runtime** becase the runtime is **proportional to the input** The **O** stands for the function we are evaluating and **(n)** stands for the size of the input.

#### O(n^2) -> Exponential runtime

```javascript
function addAndLog(array) {
  for(var i=0; i<array.length; i++) {
    for(var j=0; j<array.length; j++) {
      console.log(array[i] + array[j]);
    }
  }
}
```

This function gives us every possible combinations of pairs in the array. It iterates through the whole array and every element that it hits it goes through the whole array and hits on every element again. So, all possible pairs are made, that's why we have tow nested `for` loops.

```bash
addAndLog(['A', 'B', 'C']); # 9 pairs logged out
addAndLog(['A', 'B', 'C', 'D']); # 16 pairs logged out
addAndLog(['A', 'B', 'C', 'D', 'E']); # 25 pairs logged out
```

This is **exponential**. As we add one element to the input, the runtime makes an exponential jump. This **is not very efficient of performant**.

#### O(log n) -> Logarithmic runtime

```javascript
function binarySearch(array, key) {
  var low = 0;
  var high = array.length - 1;
  var mid;
  var element;
  while(low <= high) {
    mid = Math.floor((low + high) / 2, 10);
    element = array[mid];
    if(element < key) {
      low = mid + 1;
    }
    else if(element > key) {
      high = mid - 1;
    }
    else {
      return mid
    }
  }
  return -1;
}
```

This algotithm is very performant. In any binary search algorithm, we will have two input:

* a **list** that must be **sorted in some way**, either numerically from least to greatest of from the greatest to least, or alphabetically, etc.

* a **single value** that we want to try and search for within our array.

**Binary search** has a **logarithmic** runtime because with **every operation that we perform, we are cutting the input in half**. 

As the number of operations that we will perform will not grow proportionally but it will **grow logarithmically**.

4000 elements -> 12 operations

```bash
# Search a word in a dictionary

# source
A B C D E F G H I J K L M N O P Q R S T U V W X Y Z

# search for
"House"

# open in the middle
M

# source
A B C D E F G H I J K L

# open in the middle
F

# source
G H I J K L

# open in the middle
I

# source
G H I

# open in the middle
H

'House'

```

## Assumptions

For the purpose of this algorithm, we are going to assume that both the **noteText** and the **magazineText**:

* do not contain any type of punctuation;

*  all the letters are lowercase.

## Solution

```javascript
function harmlessRansomNote(noteText, magazineText) {

  var noteArray = noteText.split(' ');
  var magazineArray = magazineText.split(' ');
  var magazineWordCountObject = {};
  
  // O(m)
  // the (m) represents all elements of magazineArray
  magazineArray.forEach( word => {
    if (!magazineWordCountObject[word]) {
      magazineWordCountObject[word] = 0;
    }
    magazineWordCountObject[word]++;
  });

  // O(n)
  // the (n) represents how many elements are in noteArray
  var noteIsPossible = true;
  noteArray.forEach( word => {
    if(magazineWordCountObject[word]) {
      magazineWordCountObject[word]--;
      if(magazineWordCountObject[word] < 0 ){
        noteIsPossible = false;
        return;  
      }
    }
    else {
      noteIsPossible = false;
      return;
    }
  })

  console.log(noteIsPossible);
  
}
```

### Linear Time Complexity O(n)

The solution has a **linear time complexity**.

Both of the loops are run in linear time complexity but since they loop through a different array, we use different variables: `m` and `n` to describe their time complexity.

```
O(n) + O(m) = O(n + m)
```

It is **not exmponential** because in spite of we do have two loops in our function, they are not nested;
