var noteTextTest1 = 'this is a secret note for you from a secret admirer';
var noteTextTest2 = 'this is a secret note for you from an admirer';
var magazineTextTest = 'puerto rico is a great place you must hike far from town to find a secret waterfall that i am an admirer of but note that is not as hard as it seems this is my advice for you';

harmlessRansomNote(noteTextTest1, magazineTextTest);
harmlessRansomNote(noteTextTest2, magazineTextTest);

function harmlessRansomNote(noteText, magazineText) {

  var noteArray = noteText.split(' ');
  var magazineArray = magazineText.split(' ');
  var magazineWordCountObject = {};
  
  magazineArray.forEach( word => {
    if (!magazineWordCountObject[word]) {
      magazineWordCountObject[word] = 0;
    }
    magazineWordCountObject[word]++;
  });

  var noteIsPossible = true;
  noteArray.forEach( word => {
    if(magazineWordCountObject[word]) {
      magazineWordCountObject[word]--;
      if(magazineWordCountObject[word] < 0 ){
        noteIsPossible = false;
        return;  
      }
    }
    else {
      noteIsPossible = false;
      return;
    }
  })

  console.log(noteIsPossible);
  
}