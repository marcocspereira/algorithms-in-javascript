
twoSum(process.argv[2], process.argv[3]);

function twoSum(numArray, sum) {
  
  if (!numArray || !parseInt(sum)) {
    console.log('Usage: node two-sum.js \'<your-num-array-separated-with-spaces>\' <your-sum-value>');
    console.log('Example: node two-sum.js \'1 6 4 5 3 3\' 7');
    return;
  }
  numArray = numArray.split(' ');
  var result = [];
  var hashtable = [];

  for(var i=0; i<numArray.length; i++) {
    var currNum = parseInt(numArray[i]);
    var counterpart = sum - currNum;
    if (hashtable.indexOf(counterpart) !== -1) {
      result.push([currNum, counterpart]);
    }
    hashtable.push(currNum);
  }

  console.log(result)

}

