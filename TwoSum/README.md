
# Two Sum

* It takes two parameters:

  * a *numbers* **array** which we will call **numArray**;

  * **number** which we will call **sum**.

This algorithm should return an **array filled with every pair of numbers**.

## Example

```javascript
numArray = [1, 6, 4, 5, 3, 3]
sum = 7
result = [ [6, 1], [3, 4], [3,4] ]
```

## Rules

* Result shoul be an array of arrays;

* Any number in the **numArray** can be used in multiple pairs;

## Multiple ways of accomplishing result

* can be done in **O (n^2)** time complexity;

* can be done in **O (n)** time complexity.