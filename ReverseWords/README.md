# Reverse Words

This algorithm will:

* take in a string as a parameter;
* reverse every word in that string;
* return the new string.

## Rules to take in mind

* Every word should be reversed BUT the string as a whole should not be reversed;

```bash
$ reverseWords('this is a string of words')
# result
'siht si a gnirts fo sdrow'
```

* Do not use the `array.reverse()` method.