
reverseWords(process.argv.slice(2));

function reverseWords(string) {
  if (!string) {
    console.log('Usage: node reverse-words.js <your-word-or-phrase>');
    return;
  }

  string = string[0].split(' ');

  var result = string.map( word => {
    var reverseWord = '';
    for(var i= word.length-1; i>-1; i--) {
      reverseWord += word[i];
    }
    return reverseWord;
  });

  console.log(result.join(' '));

}


/*
// official implementation

function reverseWords(string) {
  var wordsArr = string.split(' ');
  var reversedWordsArr = [];
  
  wordsArr.forEach(word => {
    var reversedWord = '';
    for (var i = word.length - 1; i >= 0; i--) {
      reversedWord += word[i];
    };
    reversedWordsArr.push(reversedWord);
  });
  
  return reversedWordsArr.join(' ');
}
*/