
bubbleSort(process.argv[2]);


function bubbleSort(array) {

  if (!parseInt(array)) {
    console.log('Usage: node bubble-sort.js \'<your-num-array>\'');
    console.log('Example: node bubble-sort.js \'5 4 8 2 1 4\'');
    return;
  }


  array = array.split(' ');

  for(var i=0; i< array.length; i++) {
    array[i] = parseInt(array[i]);
  }

  for(var i=array.length; i>0; i--) {
    for(var j=0; j<i; j++) {
      if(array[j] > array[j+1]) {
        var temp = array[j];
        array[j] = array[j+1];
        array[j+1] = temp;
      }
    }
  }


  console.log(array);

}