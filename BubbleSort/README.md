
# Bubble Sort

Receives an array of numbers and returns the array sorted with bubble sort, from least to greatest.

```
[5 3 8 2 1 4]
```

* Bubble sort works by starting at the beginning of the array and looping through the whole thing.

* As we loop through the array, we compare each number with is nwighbor to the right.

* As we compare each number, we check to make sure that they are in the correct order.

``` javascript
[ 5 3 8 2 1 4 ] // original
[ 3 5 2 1 4 8 ] // step 1
// the largest number is in the correct place at the end of the array
// this is why it is called bubble sort, because the larger numbers essentially
// bubble up to the top of the array
[ 3 2 1 4 5 8 ] // step 2
[ 2 1 3 4 5 8 ] // step 3
[ 1 2 3 4 5 8 ] // final step and result
```

## Number of passes

*array.length - 1* passes