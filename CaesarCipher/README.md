# Caeser Cipher

This algorithm takes two parameters:

* a **string**

* a **number** 

The goal of Caeser Cipher algorithm is to shift every letter in our given string by the number that is passed in.

```bash
$ node caesar-cipher.js 'zoo keeper' 2
'bqq mggrgt'
```