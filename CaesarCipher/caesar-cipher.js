
caesarCipher(process.argv[2], process.argv[3]);

function caesarCipher(string, num) {
  if (!string || !parseInt(num)) {
    console.log('Usage: node caesar-cipher.js \'<your-phrase>\' <shifter>');
    return;
  }
  
  var strLower = string.toLowerCase();
  var alphabetArray = 'abcdefghijklmnopqrstuvwxyz'.split('');
  var alphabetLength = alphabetArray.length;
  
  var result = strLower.split('').map( letter => {
    var indexOfLetter = alphabetArray.indexOf(letter);
    console.log(`${letter} - ${indexOfLetter}`);
    if( indexOfLetter !== -1) {
      if((indexOfLetter + (num % alphabetLength)) > 25) {
        return alphabetArray[ (indexOfLetter + (num % alphabetLength)) - 26 ];
      }
      if((indexOfLetter + (num % alphabetLength)) < 0) {
        return alphabetArray[ (indexOfLetter + (num % alphabetLength)) + 26 ];  
      }
      return alphabetArray[ (indexOfLetter + (num % alphabetLength)) ];
    }
    return letter;
  });

  console.log(result.join(''));
}


/*
// official implementation

function caesarCipher(str,num) {
  num = num % 26;
  var lowerCaseString = str.toLowerCase();
  var alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
  var newString = '';
  
  for (var i = 0; i < lowerCaseString.length; i++) {
    var currentLetter = lowerCaseString[i];
    if (currentLetter === ' ') {
      newString += currentLetter;
      continue;
    }
    var currentIndex = alphabet.indexOf(currentLetter);
    var newIndex = currentIndex + num;
    if (newIndex > 25) newIndex = newIndex - 26;
    if (newIndex < 0) newIndex = 26 + newIndex;
    if (str[i] === str[i].toUpperCase()) {
      newString += alphabet[newIndex].toUpperCase();
    }
    else newString += alphabet[newIndex];
  };
  
  return newString;
}

caesarCipher('Zoo Keeper', 2);
*/