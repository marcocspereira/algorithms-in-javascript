# Reverse Array In Place

This algorithm will:

* take in an array as a parameter;

* reverse that array;

* return to us the reversed array.

## Rules

* Be sure to manipulate the array passed in;

* Do **not creating a new** array and pushing elements into it;

* Do **not use** `reverse()` method.