
reverseArrayInPlace(process.argv.slice(2));

function reverseArrayInPlace(array) {
  if (!array) {
    console.log('Usage: node reverse-array-in-place.js <your-array>');
    return;
  }

  array = array[0].split(' ');

  for(var i=0; i<array.length/2; i ++) {
    var temp = array[i]
    array[i] = array[array.length - 1 - i];
    array[array.length - 1 - i] = temp;
  }

  console.log(array)


}


/*
// official implementation

function reverseArrayInPlace(arr) {
  for (var i = 0; i < arr.length / 2; i++) {
    var tempVar = arr[i];
    arr[i] = arr[arr.length - 1 - i];
    arr[arr.length - 1 - i] = tempVar;
  }
  
  return arr;
}
 
reverseArrayInPlace([1, 2, 3, 4, 5, 6, 7, 8]);

*/